<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LineaInventarioController;
use App\Http\Controllers\ProductoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resources([
    'linea-inventario' => LineaInventarioController::class,
    'producto' => ProductoController::class,
]);