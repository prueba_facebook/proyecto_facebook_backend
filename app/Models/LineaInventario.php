<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LineaInventario extends Model
{
    use HasFactory;

    protected $table = "linea_inventarios";
    protected $fillable = ['cantidad', 'producto_id'];
    public $timestamps = false;

    public function producto() 
    {
        return $this->belongsTo(Producto::class, 'producto_id');
    }
}
