<?php

namespace App\Http\Controllers;

use App\Models\LineaInventario;
use App\Models\Producto;
use Exception;
use Illuminate\Http\Request;

class LineaInventarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return LineaInventario::with('producto')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->findProductoOrFail($request->get('producto_id'));
            $command = $request->only('cantidad', 'producto_id');
            return LineaInventario::create($command)->load('producto');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return $this->findOrFail($id)->load('producto');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $producto = $this->findProductoOrFail($request->get('producto_id'));
            $command = $request->only('cantidad', 'producto_id');
            $lineaInventario = $this->findOrFail($id);
            $lineaInventario->fill($command);
            $lineaInventario->save();
            return $lineaInventario->load('producto');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $lineaInventario = $this->findOrFail($id);
            $lineaInventario->delete();
            return $lineaInventario;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }


    protected function findOrFail($id)
    {
        $lineaInventario = LineaInventario::find($id);
        if (!$lineaInventario) throw new Exception("LineaInventarioNotFoundException");
        return $lineaInventario;
    }

    protected function findProductoOrFail($id)
    {
        $productoX = Producto::find($id);
        if (!$productoX) throw new Exception("ProductoNotFoundException");
        return $productoX;
    }
}
