<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use Exception;
use Illuminate\Http\Request;
use SebastianBergmann\Environment\Console;

class ProductoController extends Controller
{
       /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Producto::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $command = $request->only('nombre');
        return Producto::create($command);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return $this->findOrFail($id);
        } catch(Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $producto =  $this->findOrFail($id);
            $command = $request->only('nombre');
            $producto->fill($command);
            $producto->save();
            return $producto;
        } catch(Exception $e) {
            return $e->getMessage();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $producto = $this->findOrFail($id);
            return $producto->delete();
        } catch(Exception $e) {
            return $e->getMessage();
        }
    }

    protected function findOrFail($id) 
    {
        $productoX = Producto::find($id);
        if(!$productoX) throw new Exception("ProductoNotFoundException");
        return $productoX;
    }
}
